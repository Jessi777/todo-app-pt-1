import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
  };
  myHandleAddTodo = (e) => {
    if (e.key === "Enter") {
      let nuTodo = {
        userId: 1,
        id: Math.random() * 12000,
        title: e.target.value,
        completed: false,
      };
      console.log(nuTodo);
      this.setState((state) => ({ todos: [...state.todos, nuTodo] }));
    }
  };

  handleToggle = (id) => {
    let newTodos = this.state.todos.map((todo) => {
      if (todo.id === id) {
        return { ...todo, completed: !todo.completed };
      }
      return todo;
    });

    this.setState((state) => ({ todos: newTodos }));
  };
  handleDelete = (id) => {
    let hideTodo = this.state.todos.filter((todo) => {
      console.log(id);
      if (todo.id !== id) {
        return todo;
      }
    });
    this.setState((state) => ({ todos: hideTodo }));
  };
  clearCompleted = () => {
    console.log("test");
    let cleerComp = this.state.todos.filter((todo) => {
      if (todo.completed == false) {
        return todo;
      }
    });
    this.setState((state) => ({ todos: cleerComp }));
  };
  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input className="new-todo" placeholder="What needs to be done?" autofocus onKeyDown={this.myHandleAddTodo} />
        </header>
        <TodoList todos={this.state.todos} handleToggle={this.handleToggle} handleDelete={this.handleDelete} />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button onClick={this.clearCompleted} className="clear-completed">
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    console.log(this.props);
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
            onClick={() => this.props.handleToggle(this.props.id)}
          />
          <label>{this.props.title}</label>
          <button className="destroy" onClick={() => this.props.handleDelete(this.props.id)} />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              handleToggle={this.props.handleToggle}
              handleDelete={this.props.handleDelete}
              id={todo.id}
              title={todo.title}
              completed={todo.completed}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
